#!/usr/bin/env python3

# 
# This installs the module dumux-pnm-efi-r and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |   branch name   |                 commit sha                 |         commit date         |
# |-----------------------|-----------------|--------------------------------------------|-----------------------------|
# |       dune-grid       |  origin/master  |  8a6f62335d454a7055f0abefd1b8f7e007bed4bb  |  2023-01-27 22:00:29 +0000  |
# |     dune-geometry     |  origin/master  |  4e561fdb5d839fbbf528fa58897ba31e159207e4  |  2023-01-31 20:52:56 +0000  |
# |     dune-functions    |  origin/master  |  20d59c97bc0e19a5753ff38474752880d6eac0fd  |  2023-02-10 19:08:46 +0000  |
# |      dune-uggrid      |  origin/master  |  d0762d9b1c56048ab738eba80dbdd65604b26708  |  2022-11-28 16:10:22 +0000  |
# |      dune-alugrid     |  origin/master  |  13f131263f9293d768fa5a542a9c52bd3e432c5f  |  2023-02-13 11:58:52 +0100  |
# |     dune-foamgrid     |  origin/master  |  4845fbcac1bb814fc65173dfd2f4d1d58ce33863  |  2023-02-02 11:36:13 +0000  |
# |      dune-subgrid     |  origin/master  |  e83f3f919c2602425467ed767f279bc9c356c436  |  2023-01-04 19:24:06 +0000  |
# |     dune-typetree     |  origin/master  |  d3345d13804d9b6a26973272d799ca345abfd49c  |  2022-12-01 09:56:36 +0000  |
# |       dune-istl       |  origin/master  |  1623ad6e3b5a55a8b7343dfdcb61ac47bf821b2e  |  2023-02-12 20:05:08 +0000  |
# |      dune-common      |  origin/master  |  b4e28a193276df89509a90edc269bde92a18a8c5  |  2023-02-22 12:03:54 +0000  |
# |  dune-localfunctions  |  origin/master  |  3cf395ef33a3a0241c5ca705d19aa364b4b1c283  |  2023-02-02 09:14:56 +0000  |

import os
import sys
import subprocess

top = "."
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/master", "8a6f62335d454a7055f0abefd1b8f7e007bed4bb", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/master", "4e561fdb5d839fbbf528fa58897ba31e159207e4", )

print("Installing dune-functions")
installModule("dune-functions", "https://gitlab.dune-project.org/staging/dune-functions.git", "origin/master", "20d59c97bc0e19a5753ff38474752880d6eac0fd", )

print("Installing dune-uggrid")
installModule("dune-uggrid", "https://gitlab.dune-project.org/staging/dune-uggrid.git", "origin/master", "d0762d9b1c56048ab738eba80dbdd65604b26708", )

print("Installing dune-alugrid")
installModule("dune-alugrid", "https://gitlab.dune-project.org/extensions/dune-alugrid.git", "origin/master", "13f131263f9293d768fa5a542a9c52bd3e432c5f", )

print("Installing dune-foamgrid")
installModule("dune-foamgrid", "https://gitlab.dune-project.org/extensions/dune-foamgrid.git", "origin/master", "4845fbcac1bb814fc65173dfd2f4d1d58ce33863", )

print("Installing dune-subgrid")
installModule("dune-subgrid", "https://gitlab.dune-project.org/extensions/dune-subgrid.git", "origin/master", "e83f3f919c2602425467ed767f279bc9c356c436", )

print("Installing dune-typetree")
installModule("dune-typetree", "https://gitlab.dune-project.org/staging/dune-typetree.git", "origin/master", "d3345d13804d9b6a26973272d799ca345abfd49c", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/master", "1623ad6e3b5a55a8b7343dfdcb61ac47bf821b2e", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/master", "b4e28a193276df89509a90edc269bde92a18a8c5", )

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/master", "3cf395ef33a3a0241c5ca705d19aa364b4b1c283", )

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=dumux-pnm-efi-r/cmake.opts', 'all'],
    '.'
)
