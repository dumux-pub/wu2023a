#!/bin/bash

# 
# This installs the module dumux-pnm-efi-r and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |   branch name   |                 commit sha                 |         commit date         |
# |-----------------------|-----------------|--------------------------------------------|-----------------------------|
# |       dune-grid       |  origin/master  |  8a6f62335d454a7055f0abefd1b8f7e007bed4bb  |  2023-01-27 22:00:29 +0000  |
# |     dune-geometry     |  origin/master  |  4e561fdb5d839fbbf528fa58897ba31e159207e4  |  2023-01-31 20:52:56 +0000  |
# |      dune-uggrid      |  origin/master  |  d0762d9b1c56048ab738eba80dbdd65604b26708  |  2022-11-28 16:10:22 +0000  |
# |     dune-foamgrid     |  origin/master  |  4845fbcac1bb814fc65173dfd2f4d1d58ce33863  |  2023-02-02 11:36:13 +0000  |
# |       dune-istl       |  origin/master  |  1623ad6e3b5a55a8b7343dfdcb61ac47bf821b2e  |  2023-02-12 20:05:08 +0000  |
# |      dune-common      |  origin/master  |  b4e28a193276df89509a90edc269bde92a18a8c5  |  2023-02-22 12:03:54 +0000  |
# |  dune-localfunctions  |  origin/master  |  3cf395ef33a3a0241c5ca705d19aa364b4b1c283  |  2023-02-02 09:14:56 +0000  |

exitWithError()
{
    MSG=$1
    echo "$MSG"
    exit 1
}

installModule()
{
    FOLDER=$1
    URL=$2
    BRANCH=$3
    REVISION=$4

    if [ ! -d "$FOLDER" ]; then
        if ! git clone $URL; then exitWithError "clone failed"; fi
        pushd $FOLDER
            if ! git checkout $BRANCH; then exitWithError "checkout failed"; fi
            if ! git reset --hard $REVISION; then exitWithError "reset failed"; fi
        popd
    else
        echo "Skip cloning $URL since target folder "$FOLDER" already exists."
    fi
}

applyPatch()
{
    FOLDER=$1
    PATCH=$2

    pushd $FOLDER
        echo "$PATCH" > tmp.patch
        if ! git apply tmp.patch; then exitWithError "patch failed"; fi
        rm tmp.patch
    popd
}

TOP="."
mkdir -p $TOP
cd $TOP

echo "Installing dune-grid"
installModule dune-grid https://gitlab.dune-project.org/core/dune-grid.git origin/master 8a6f62335d454a7055f0abefd1b8f7e007bed4bb
echo "Installing dune-geometry"
installModule dune-geometry https://gitlab.dune-project.org/core/dune-geometry.git origin/master 4e561fdb5d839fbbf528fa58897ba31e159207e4
echo "Installing dune-uggrid"
installModule dune-uggrid https://gitlab.dune-project.org/staging/dune-uggrid.git origin/master d0762d9b1c56048ab738eba80dbdd65604b26708
echo "Installing dune-foamgrid"
installModule dune-foamgrid https://gitlab.dune-project.org/extensions/dune-foamgrid.git origin/master 4845fbcac1bb814fc65173dfd2f4d1d58ce33863
echo "Installing dune-istl"
installModule dune-istl https://gitlab.dune-project.org/core/dune-istl.git origin/master 1623ad6e3b5a55a8b7343dfdcb61ac47bf821b2e
echo "Installing dune-common"
installModule dune-common https://gitlab.dune-project.org/core/dune-common.git origin/master b4e28a193276df89509a90edc269bde92a18a8c5
echo "Installing dune-localfunctions"
installModule dune-localfunctions https://gitlab.dune-project.org/core/dune-localfunctions.git origin/master 3cf395ef33a3a0241c5ca705d19aa364b4b1c283
echo "Installing dumux"
installModule dumux https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git feature/pnm-reg-2pnc-results 85d8f5087d01e69cde9cbdbdb3e39a55c5011a0f

echo "Configuring project"
if ! ./dune-common/bin/dunecontrol --opts=dumux-pnm-efi-r/cmake.opts all; then
    echo "Configuration of the project failed"
    exit 1
fi
