This file has been created automatically. Please adapt it to your needs.

## Content

The content of this DUNE module was extracted from the module `dumux`.
In particular, the following subFolder of `dumux` have been extracted:
*   `test/porenetwork/2p/`


Additionally, all headers in `dumux` that are required to build the
executables from the sources
*   `test/porenetwork/2p/pcswcurve/main_dynamic.cc`
*   `test/porenetwork/2p/pcswcurve/main_static.cc`
*   `test/porenetwork/2p/3d_comparison_study/main.cc`
*   `test/porenetwork/2p/static/main.cc`
*   `test/porenetwork/2p/1d_dynamic/main.cc`
*   `test/porenetwork/2p/1d_dynamic/main_plot_constitutive_laws.cc`


have been extracted. You can configure the module just like any other DUNE
module by using `dunecontrol`. For building and running the executables,
please go to the build folders corresponding to the sources listed above.


## License

This project is licensed under the terms and conditions of the GNU General Public
License (GPL) version 3 or - at your option - any later version.
The GPL can be found under [GPL-3.0-or-later.txt](LICENSES/GPL-3.0-or-later.txt)
provided in the `LICENSES` directory located at the topmost of the source code tree.


## Version Information

|      module name      |   branch name   |                 commit sha                 |         commit date         |
|-----------------------|-----------------|--------------------------------------------|-----------------------------|
|       dune-grid       |  origin/master  |  8a6f62335d454a7055f0abefd1b8f7e007bed4bb  |  2023-01-27 22:00:29 +0000  |
|     dune-geometry     |  origin/master  |  4e561fdb5d839fbbf528fa58897ba31e159207e4  |  2023-01-31 20:52:56 +0000  |
|      dune-uggrid      |  origin/master  |  d0762d9b1c56048ab738eba80dbdd65604b26708  |  2022-11-28 16:10:22 +0000  |
|     dune-foamgrid     |  origin/master  |  4845fbcac1bb814fc65173dfd2f4d1d58ce33863  |  2023-02-02 11:36:13 +0000  |
|       dune-istl       |  origin/master  |  1623ad6e3b5a55a8b7343dfdcb61ac47bf821b2e  |  2023-02-12 20:05:08 +0000  |
|      dune-common      |  origin/master  |  b4e28a193276df89509a90edc269bde92a18a8c5  |  2023-02-22 12:03:54 +0000  |
|  dune-localfunctions  |  origin/master  |  3cf395ef33a3a0241c5ca705d19aa364b4b1c283  |  2023-02-02 09:14:56 +0000  |

## Installation

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_dumux-pnm-efi-r.sh`
provided in this repository to install all dependent modules.


```sh
mkdir DUMUX
cd DUMUX
git clone https://git.iws.uni-stuttgart.de/dumux-pub/wu2023a.git dumux-pnm-efi-r
./dumux-pnm-efi-r/install_dumux-pnm-efi-r.sh
```

This will clone all modules into the directory `DUMUX`,
configure your module with `dunecontrol` and build tests.

